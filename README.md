manifest
========

[MPL-2] a global manifest interface to download all the reopository

Install the manifest manager tool:
=================================

simple python tools:


Install Island tool:
```{.sh}
pip install island --user
```

If you do not have pip on ubuntu:
```{.sh}
sudo apt install python-pip
echo "export PATH=$PATH:/home/$USER/.local/bin/" >> /home/$USER/.bashrc
source /home/$USER/.bashrc
```

Download all the reopository
===========================

This is simple:

```{.bash}
# create the workspace directory
mkdir WORKSPACE
cd WORKSPACE
# initialize the workspace
island init git@gitlab.com:scenarium/manifest.git
# download all the sources
island sync

cd ..
```

Or use generic HTTP interface:

```{.bash}
# create the workspace directory
mkdir WORKSPACE
cd WORKSPACE
# initialize the workspace
island init http://gitlab.com/scenarium/manifest.git
# download all the sources
island sync

cd ..
```

Simple command about island
==========================

Check your project status:
```{.sh}
cd WORKSAPCE
island status
cd ..
```

Select the developement branch:
```{.sh}
cd WORKSAPCE
island checkout develop
cd ..
```




